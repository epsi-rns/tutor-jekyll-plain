# Jekyll Test Drive

An example of pure Jekyll site without burden of any stylesheet
for personal learning purpose.

> Jekyll (Liquid)

If you want to see eye candy theme,
you may consider to have a look at,
Jekyll with CSS Framework,
with two link choices below.

-- -- --

## Jekyll Version

Since this repository is still using Jekyll 3.8, instead of Jekyll 4.0,
you might need to run `bundle install`, depend on the situation.

	$ bundle install

-- -- --

## Jekyll Step By Step

This Tutorial is made in two stages.

1. Jekyll in plain HTML:
   covering liquid, avoiding burden of any stylesheet

2. Jekyll with CSS Frameworks:
   with two choices, either Bulma or Bootstrap.

### Stage 1: Plain

> This repository:

* [Jekyll Step by Step Repository][tutorial-jekyll-plain]

Contain Tutor-01 until Tutor-05.
With additional Tutor-06 to create plugins,
and Tutor-07 to create basic theme in Gem.

### Stage 2: CSS Frameworks

Two implementation choices.

* [Jekyll (Bulma MD)][tutorial-jekyll-bmd]

* [Jekyll (Bootstrap OC)][tutorial-jekyll-boc]

Each contain Tutor-08 until Tutor-12.
With additional Tutor-13 as a complete site with stylesheet and plugins,
and Tutor-14 as complete theme in Gem.

### Jekyll Tutorial

A thorough Jekyll tutorial can be found in this article series:

* [Jekyll - Overview][series-jekyll]

![Jekyll Plain: Article Series][jekyll-plain-articles]

[tutorial-jekyll-bmd]:  https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/
[tutorial-jekyll-boc]:  https://gitlab.com/epsi-rns/tutor-jekyll-bootstrap-oc/
[tutorial-jekyll-plain]:https://gitlab.com/epsi-rns/tutor-jekyll-plain/
[series-jekyll]:        https://epsi-rns.gitlab.io/ssg/2020/06/01/jekyll-overview/

-- -- --

## Chapter Step by Step

### Tutor 01

> Generate Only Pure HTML

* Configuration: Minimal

* General Layout: Default

![Jekyll Plain: Tutor 01][jekyll-plain-preview-01]

-- -- --

### Tutor 02

> Include Liquid Partials

* Setup Directory for Minimal Jekyll

* New Layout: [Page, Post, Home] (extends Default)

* Partials: HTML Head, Header, Footer

* Basic Content

![Jekyll Plain: Tutor 02][jekyll-plain-preview-02]

-- -- --

### Tutor 03

> Loop with Liquid

* More Content: Lyrics. This is required for loop demo

* Index Content: Index, Category, Tag, Archive By Year/Month

![Jekyll Plain: Tutor 03][jekyll-plain-preview-03]

-- -- --

### Tutor 04

> Template Inheritance in Liquid

* Custom Output: YAML, JSON

* List Tree: Archives: By Year, By Year and Month

* List Tree: Tags and Categories

* Partials: Blog List, Terms, Archive

* New Layout: Index (extends Page)

* Markdown

* Shortcode

![Jekyll Plain: Tutor 04][jekyll-plain-preview-04]

-- -- --

### Tutor 05

> Pagination with Liquid

* Pagination (v1): Simple, Number, Adjacent, Indicator

* Pagination (v2): Simple, Number, Adjacent, Indicator

* Meta HTML

* Post Navigation

![Jekyll Plain: Tutor 05][jekyll-plain-preview-05]

-- -- --

### Tutor 06

> Jekyll Plugin with Ruby

* Filter: Year Text

* Filter: Term Array

* Filter: Pagination Links, Link Offset Flag

* Filter: Keywords

* Generator: Tag Names

![Jekyll Plain: Tutor 06][jekyll-plain-preview-06]

-- -- --

### Tutor 07

> Jekyll Theme

* Bundling Gem

* Using Theme

![Jekyll Plain: Tutor 07][jekyll-plain-preview-07]

-- -- --

## Additional Links

### HTML Step By Step

Additional guidance:

* [Bulma Material Design Step by Step Repository][tutorial-bulma-md] (frankenbulma)

* [Bulma Step by Step Repository][tutorial-bulma]

* [Materialize Step by Step Repository][tutorial-materialize]

### Comparison

Comparation with other static site generator

* [Eleventy (Materialize) Step by Step Repository][tutorial-11ty-m]

* [Eleventy (Bulma MD) Step by Step Repository][tutorial-11ty-b]

* [Hugo Step by Step Repository][tutorial-hugo]

* [Hexo Step by Step Repository][tutorial-hexo]

* [Pelican Step by Step Repository][tutorial-pelican]

### Presentation

* [Concept SSG - Presentation Slide][ssg-presentation]

* [Concept CSS - Presentation Slide][css-presentation]

[tutorial-pelican]:     https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/
[tutorial-hugo]:        https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/
[tutorial-11ty-m]:      https://gitlab.com/epsi-rns/tutor-11ty-materialize/
[tutorial-11ty-b]:      https://gitlab.com/epsi-rns/tutor-11ty-bulma-md/
[tutorial-hexo]:        https://gitlab.com/epsi-rns/tutor-hexo-bulma/

[tutorial-bulma-md]:    https://gitlab.com/epsi-rns/tutor-html-bulma-md/
[tutorial-bulma]:       https://gitlab.com/epsi-rns/tutor-html-bulma/
[tutorial-materialize]: https://gitlab.com/epsi-rns/tutor-html-materialize/

[ssg-presentation]:     https://epsi-rns.gitlab.io/ssg/2019/02/17/concept-ssg/
[css-presentation]:     https://epsi-rns.gitlab.io/frontend/2019/02/15/concept-css/

-- -- --

## What's Next?

You can continue with either Bootstrap or Bulma.

### Bootstrap Preview

An example of Jekyll site using Bootstrap Open Color
for personal learning purpose.

> Jekyll (Liquid) + Bootstrap (Open Color)

![Jekyll Plain: Tutor][jekyll-bootstrap-preview]

### Bulma Preview

An example of Jekyll site using Bulma Material Design
for personal learning purpose.

> Jekyll (Liquid) + Bulma (Material Design)

![Jekyll Bulma: Tutor][jekyll-bulma-preview]

-- -- --

What do you think ?

[jekyll-plain-preview]:     https://gitlab.com/epsi-rns/tutor-jekyll-plain/raw/master/jekyll-plain-preview.png
[jekyll-plain-articles]:    https://gitlab.com/epsi-rns/tutor-jekyll-plain/raw/master/toc-jekyll-all.png
[jekyll-plain-preview-01]:  https://gitlab.com/epsi-rns/tutor-jekyll-plain/raw/master/tutor-01/jekyll-plain-preview.png
[jekyll-plain-preview-02]:  https://gitlab.com/epsi-rns/tutor-jekyll-plain/raw/master/tutor-02/jekyll-plain-preview.png
[jekyll-plain-preview-03]:  https://gitlab.com/epsi-rns/tutor-jekyll-plain/raw/master/tutor-03/jekyll-plain-preview.png
[jekyll-plain-preview-04]:  https://gitlab.com/epsi-rns/tutor-jekyll-plain/raw/master/tutor-04/jekyll-plain-preview.png
[jekyll-plain-preview-05]:  https://gitlab.com/epsi-rns/tutor-jekyll-plain/raw/master/tutor-05/jekyll-plain-preview.png
[jekyll-plain-preview-06]:  https://gitlab.com/epsi-rns/tutor-jekyll-plain/raw/master/tutor-06/jekyll-plain-preview.png
[jekyll-plain-preview-07]:  https://gitlab.com/epsi-rns/tutor-jekyll-plain/raw/master/tutor-07/jekyll-plain-preview.png

[jekyll-bootstrap-preview]: https://gitlab.com/epsi-rns/tutor-jekyll-bootstrap-oc/raw/master/jekyll-bootstrap-oc-preview.png
[jekyll-bulma-preview]:     https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/raw/master/jekyll-bulma-md-preview.png

