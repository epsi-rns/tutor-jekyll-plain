### Tutor 01

> Generate Only Pure HTML

* Configuration: Minimal

* General Layout: Default

![Jekyll Plain: Tutor 01][jekyll-plain-preview-01]

-- -- --

What do you think ?

[jekyll-plain-preview-01]:  https://gitlab.com/epsi-rns/tutor-jekyll-plain/raw/master/tutor-01/jekyll-plain-preview.png
