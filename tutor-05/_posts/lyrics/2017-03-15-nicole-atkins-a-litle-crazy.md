---
layout      : post
title       : Nicole Atkins -  A Little Crazy
date        : 2017-03-15 07:35:05 +0700
categories  : lyric
tags        : [soul, 2010s]
author      : Nicole Atkins

excerpt     :
  Smilin, did you see me smiling?  
  The whole act's just for you.
  What else can I do?

opengraph:
  image: https://t2.genius.com/unsafe/220x220/https%3A%2F%2Fimages.genius.com%2Ff03b813d36978fd8f5f635ab860f91c1.1000x1000x1.jpg
---

Walkin, spent a lot of time walkin  
Cause that's what people do

Smilin, did you see me smiling?  
The whole act's just for you  
What else can I do?

People say I'm better of alone  
People say it takes a little time  
I can't hear a word those people say  
I know that I'm crazy, crazy, crazy
