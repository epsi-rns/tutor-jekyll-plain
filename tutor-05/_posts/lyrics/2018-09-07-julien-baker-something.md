---
layout      : post
title       : Julien Baker - Something
date        : 2018-09-07 07:35:05 +0700
categories  : lyric
tags        : [rock, 2010s]
keywords    : [sad, emo, broken]
author      : Julien Baker

excerpt     :
  I can't think of anyone, anyone else.  
  I won't think of anyone else.

opengraph:
  image: https://t2.genius.com/unsafe/220x220/https%3A%2F%2Fimages.genius.com%2Fad35fd5b6e1e7808990ebcc93e8179b9.1000x1000x1.jpg
---

I knew I was wasting my time.  
Keep myself awake at night.  
'Cause whenever I close my eyes.  
I'm chasing your tail lights.  
In the dark.  
In the dark.  
In the dark.

Watched you said nothing, said nothing, said nothing.  
I can't think of anyone, anyone else.  
I can't think of anyone, anyone else.  
I can't think of anyone, anyone else.  
I won't think of anyone else.
