---
layout      : post
title       : Julien Baker - Sprained Ankle
date        : 2018-09-13 07:35:05 +0700
categories  : lyric
tags        : [rock, 2010s]
keywords    : [sad, emo, broken]
author      : Julien Baker

excerpt: Wish I could write songs about anything other than death

excerpt: Wish I could write songs about anything other than death

opengraph:
  image: https://t2.genius.com/unsafe/220x220/https%3A%2F%2Fimages.genius.com%2Fad35fd5b6e1e7808990ebcc93e8179b9.1000x1000x1.jpg
---

A sprinter learning to wait  
A marathon runner, my ankles are sprained  
A marathon runner, my ankles are sprained
