### Tutor 06

> Jekyll Plugin with Ruby

* Filter: Year Text

* Filter: Term Array

* Filter: Pagination Links, Link Offset Flag

* Filter: Keywords

* Generator: Tag Names

![Jekyll Plain: Tutor 06][jekyll-plain-preview-06]

-- -- --

What do you think ?

[jekyll-plain-preview-06]:  https://gitlab.com/epsi-rns/tutor-jekyll-plain/raw/master/tutor-06/jekyll-plain-preview.png
