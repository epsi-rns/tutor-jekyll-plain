module Jekyll
  module TextYear
    def text_year(post_year)
      (post_year == Time.now.strftime("%Y")) ?
        "This year's posts (#{post_year})" : post_year
    end
  end
end

Liquid::Template.register_filter(Jekyll::TextYear)

=begin
    def text_year(post_year)
      localtime = Time.now
      current_year = localtime.strftime("%Y")

      if post_year == current_year
        year_text = "This year's posts (#{post_year})"
      else
        year_text = post_year
      end

      year_text
    end
=end
