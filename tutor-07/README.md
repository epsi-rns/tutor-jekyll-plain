### Tutor 07

> Jekyll Theme

* Bundling Gem

* Using Theme

![Jekyll Plain: Tutor 07][jekyll-plain-preview-07]

-- -- --

What do you think ?

[jekyll-plain-preview-07]:  https://gitlab.com/epsi-rns/tutor-jekyll-plain/raw/master/tutor-07/jekyll-plain-preview.png
