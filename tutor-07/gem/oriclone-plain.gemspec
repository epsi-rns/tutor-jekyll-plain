Gem::Specification.new do |spec|
  spec.name        = 'oriclone-plain'
  spec.version     = '0.1.0'
  spec.date        = '2020-07-06'
  spec.authors     = ["E.R. Nurwijayadi"]
  spec.email       = 'epsi.nurwijayadi@gmail.com'

  spec.summary     = "Oriclone Plain - Jekyll Theme"
  spec.description = "An example of Jekyll Theme without Stylesheet Burden"
  spec.homepage    = "https://gitlab.com/epsi-rns/tutor-jekyll-plain"
  spec.license     = "MIT"

  require 'rake'
  spec.files       = Dir.glob("{_layouts,_includes}/**/**/*") +
                     ['LICENSE']
end
